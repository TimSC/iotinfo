import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "iotinfo.settings")
import django
django.setup()
from control import models
import csv

if __name__=="__main__":
	
	data = csv.DictReader(open("/home/tim/Desktop/homez/data.csv"))

	for cat in models.Category.objects.all():
		cat.delete()	
	for cat in models.Device.objects.all():
		cat.delete()
	for cat in models.Metric.objects.all():
		cat.delete()
	for cat in models.Note.objects.all():
		cat.delete()

	c = models.Category(name="Home")
	c.save()

	for i, li in enumerate(data):
		li = dict(li)
		print (li)
		if i == 0:
			for k in li:
				if k == "Name": continue
				m = models.Metric(name=k)
				m.save()
		
		d = models.Device(name=li['Name'], category = c)		
		d.save()

		for k in li:
			if k == "Name": continue
			m = models.Metric.objects.filter(name=k).first()
			n = models.Note(device=d, metric=m, weight=int(li[k]), txt='')
			n.save()		

