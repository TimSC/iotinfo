from django.shortcuts import render
from control import models

# Create your views here.

def vis(request):

	response_data = {}

	cat_data = []
	for cat in models.Category.objects.all():
		cat_data.append({'id': cat.id, 'name': cat.name})
	response_data['category_list'] = cat_data

	device_data = []
	for dev in models.Device.objects.all():
		device_data.append({'id': dev.id, 'name': dev.name, 'category': dev.category.id})
	response_data['device_list'] = device_data

	metric_data = []
	for met in models.Metric.objects.all():
		metric_data.append({'id': met.id, 'name': met.name})
	response_data['metric_list'] = metric_data

	note_data = []
	for no in models.Note.objects.all():
		note_data.append({'id': no.id, 'txt': no.txt, 'device': no.device.id, 'metric': no.metric.id, 'weight': no.weight})
	response_data['note_list'] = note_data


	return render(request, "frontend/vis.html", response_data)

