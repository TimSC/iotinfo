from django.shortcuts import render
from django.http  import HttpResponse
import json
from control import models
# Create your views here.

def test(request):
	response_data = {}

	cat_data = []
	for cat in models.Category.objects.all():
		cat_data.append({'id': cat.id, 'name': cat.name})
	response_data['category'] = cat_data

	device_data = []
	for dev in models.Device.objects.all():
		device_data.append({'id': dev.id, 'name': dev.name, 'category': dev.category.id})
	response_data['device'] = device_data

	metric_data = []
	for met in models.Metric.objects.all():
		metric_data.append({'id': met.id, 'name': met.name})
	response_data['metric'] = metric_data

	note_data = []
	for no in models.Note.objects.all():
		note_data.append({'id': no.id, 'txt': no.txt, 'device': no.device.id, 'metric': no.metric.id})
	response_data['note'] = note_data

	return HttpResponse(json.dumps(response_data), content_type="application/json")

