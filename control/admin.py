from django.contrib import admin
from control import models

# Register your models here.

admin.site.register(models.Category)
admin.site.register(models.Device)
admin.site.register(models.Note)
admin.site.register(models.Metric)

