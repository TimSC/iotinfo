from django.db import models

# Create your models here.

class Category(models.Model):
	name = models.CharField(max_length=30)

	def __repr__(self):
		return self.name

	def __str__(self):
	   return '{}: {}'.format(self.__class__.__name__, self.name)

class Device(models.Model):
	name = models.CharField(max_length=30)

	category = models.ForeignKey(Category, on_delete=models.CASCADE)

	def __repr__(self):
		return self.name
	
	def __str__(self):
	   return '{}: {}'.format(self.__class__.__name__, self.name)

class Metric(models.Model):
	name = models.CharField(max_length=30)
	
	def __str__(self):
	   return '{}: {}'.format(self.__class__.__name__, self.name)

class Note(models.Model):
	weight = models.FloatField()
	txt = models.TextField()

	device = models.ForeignKey(Device, on_delete=models.CASCADE)

	metric = models.ForeignKey(Metric, on_delete=models.CASCADE)

	def __str__(self):
	   return '{}: {}'.format(self.__class__.__name__, self.id)

